FROM ubuntu:18.04
LABEL MAINTAINER=ekaterina.mikhaylova@positrigo.com

ENV CS_TAR="coincidence-sorter.tar.gz"
ENV ROOT_TAR="root_v6.08.06.source.tar.gz"
ENV FOLDER="/bop"
ENV DISPLAY=":0"
#ENV LD_LIBRARY_PATH=/bop/root/build/lib:$LD_LIBRARY_PATH

RUN apt-get update && apt-get upgrade -y

# Install dependencies
# --no-install-recommends
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
    dpkg-dev make g++ gcc \
    binutils libx11-dev libxpm-dev libxft-dev \
    libxext-dev libpng-dev libjpeg-turbo8-dev \
    python gfortran libssl-dev \
    libboost-all-dev cmake doxygen libyaml-cpp-dev


    #cmake cmake-doc cmake-data texlive graphviz dpkg-dev g++ gcc binutils libx11-dev \
    #libxpm-dev libxft-dev libxext-dev libgsl0-dev python-pip libxerces-c-dev libqt4-dev \
    #libxt-dev libxmu-dev gcc g++ make libncurses-dev libx11-dev libboost-dev libpng-dev \
    #python python-dev ipython python-matplotlib mpi-default-dev \
    #mpi-default-bin libinsighttoolkit4-dev libtiff5-dev python-py python-instant \
    #libboost-all-dev qt4-dev-tools doxygen libyaml-cpp-dev libgdcm-cil libgdcm-java \
    #libgdcm-tools libgdcm2-dev libgdcm2.8 python-gdcm \
    #libvtkgdcm-cil libvtkgdcm-java libvtkgdcm-tools libvtkgdcm2-dev libvtkgdcm2.8 python-vtkgdcm \
    #tcsh swig mayavi2 libqt4-opengl-dev \
    #python-setuptools python-tk

#RUN apt-get install -y libfftw3-dev libfftw3-doc doxygen swig cmake-curses-gui

RUN ldconfig

#---------- Build ROOT ----------
WORKDIR $FOLDER/root/
COPY $ROOT_TAR $FOLDER/root/. 
RUN tar -xvf $ROOT_TAR
RUN rm $ROOT_TAR
WORKDIR $FOLDER/root/build/
RUN cmake ../root-6.08.06/
RUN make -j 6
RUN make install -j 6

RUN echo " " >> /root/.bashrc
RUN echo "# for ROOT" >> /root/.bashrc
RUN echo "export ROOTSYS=$FOLDER/root/build/" >> /root/.bashrc
RUN echo "PATH=/bop/root/build/bin/:\$PATH" >> /root/.bashrc
RUN echo "LD_LIBRARY_PATH=/bop/root/build/lib:\$LD_LIBRARY_PATH" >> /root/.bashrc
RUN echo "export DISPLAY=0" >> /root/.bashrc
RUN echo "source $FOLDER/root/build/bin/thisroot.sh" >> /root/.bashrc
RUN echo " " >> /root/.bashrc

RUN /bin/bash -c "source /root/.bashrc"

# ----- Build and install csorter ------
WORKDIR $FOLDER/cs/
COPY $CS_TAR $FOLDER/cs/.
RUN tar -xvzf $CS_TAR
RUN rm $CS_TAR
WORKDIR $FOLDER/cs/build/
RUN cmake ../coincidence-sorter/ -DUSE_CERN_ROOT=ON
RUN make -j 6

RUN echo "# for cs" >> /root/.bashrc
RUN echo "PATH=$FOLDER/cs/build/utilities:\$PATH" >> /root/.bashrc

WORKDIR /tmp/

RUN /bin/bash -c "source /root/.bashrc"

WORKDIR /root/.local/bin/
COPY init /root/.local/bin/.
WORKDIR /tmp/
ENV LD_LIBRARY_PATH=/bop/root/build/lib:$LD_LIBRARY_PATH
ENTRYPOINT ["bash","/root/.local/bin/init"]




